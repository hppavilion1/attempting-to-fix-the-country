# Presidential Recall Elections Amendment
## Section 1
The House shall have the power to subject the President to recall elections with the consent of one third of its seats. Procedure for recall elections shall be prescribed by the Congress.

## Section 2
No president shall be subject to recall elections initiated through Section 1 of this amendment within six months of taking office, or more than once within a one year period.