# Class A Problems
*Fundamental, structural problems with the process used to administer the country, created by the powers granted to specific individuals or small groups*

- Trumps: Individuals in the government amassing too much power, becoming dictatorial
- McConnells: Obstructionists in the government preventing it from carrying out necessary actions

# Class B Problems
*Simple systemic problems with the government which compromise the democratic character of the country, but which can't be so easily pinned on individuals*

- The Electoral College
- Gerrymandering
- U.S. Territories

# Class C Problems
*Further systemic issues compromsing the country's democratic character*

- First-Past-the-Post elections
- Non-proportional representation in legislatures

# Class D Problems
*Problems that could lead to potentially-serious issues in the future, but which are yet to come up*

- Overhaul Continuity of Government for all three branches of government

# Class E Problems
*Problems involving rights being underprotected*

- Lack of constitutionally-guaranteed equal treatment with regards to:
	- Sex/Gender
	- Race/Ethnicity
	- Religion
	- National Origin
	- Sexual Orientation
	- Gender Identity